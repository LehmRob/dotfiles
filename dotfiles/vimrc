set ai
set hidden
set icon
set expandtab
set relativenumber
set number
set showmatch
set showcmd
set incsearch
set ic
set hlsearch
set sw=2
set ts=2
set sts=2
set t_Co=256

" Note: Skip initialization for vim-tiny or vim-small.
if 0 | endif

if &compatible
    set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=~/.vim/bundle/neobundle.vim/

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" Add or remove your Bundles here:
NeoBundle 'Shougo/neosnippet.vim'
NeoBundle 'flazz/vim-colorschemes'
NeoBundle 'fatih/vim-go'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'scrooloose/nerdcommenter'
NeoBundle 'rhysd/vim-clang-format'
NeoBundle 'rking/ag.vim'
NeoBundle 'ervandew/supertab'

call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck

syntax enable
set t_Co=256
set background=light

" Misc settings
let g:go_fmt_command = "goimports"
let g:clang_format#detect_style_file = 1
let g:clang_format#auto_format = 1

autocmd FileType make setlocal noexpandtab
autocmd FileType c ClangFormatAutoEnable
filetype indent on

let mapleader = " "
map <Leader>s :split<cr>
map <Leader>v :vsplit<cr>
map <Leader>w <C-w>w
map <Leader>a :q!<cr>
map <Leader>s :w<cr>
map <Leader>q :wq<cr>
map <Leader>nt :NERDTreeToggle<cr>
map <Leader>t :tabedit<space>
map <Leader>cw ::%s/\s\+$//e<cr>:w<cr>
map <Leader>b :buffers<CR>:buffer<Space>
imap <a-,> <ESC>

set statusline=
set statusline +=\ %n\ %*            "buffer number
set statusline +=%{&ff}%*            "file format
set statusline +=%y%*                "file type
set statusline +=\ %<%f%*            "full path
set statusline +=%m%*                "modified flag
set statusline +=%=%5l%*             "current line
set statusline +=/%L%*               "total lines
set statusline +=%4v\ %*             "virtual column number
set laststatus=2

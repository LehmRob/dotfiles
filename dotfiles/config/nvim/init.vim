set termguicolors
set ai
set hidden
set icon
set expandtab
set relativenumber
set number
set showmatch
set showcmd
set incsearch
set ic
set hlsearch
set sw=2
set ts=2
set sts=2

autocmd FileType make setlocal noexpandtab
autocmd FileType c ClangFormatAutoEnable
filetype indent on

let mapleader = ","
map <Leader>s :split<cr>
map <Leader>v :vsplit<cr>
map <Leader>w <C-w>w
map <Leader>a :q!<cr>
map <Leader>s :w<cr>
map <Leader>q :wq<cr>
map <Leader>t :tabedit<space>
map <Leader>b :buffers<CR>:buffer<Space>
imap <a-,> <ESC>

if has('vim_starting')
  " Required:
  set runtimepath+=/home/lehmrob/.config/nvim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('/home/lehmrob/.config/nvim/bundle'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" Add or remove your Bundles here:
NeoBundle 'Shougo/neosnippet.vim'
NeoBundle 'Shougo/neosnippet-snippets'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'ctrlpvim/ctrlp.vim'
NeoBundle 'flazz/vim-colorschemes'
NeoBundle 'rust-lang/rust.vim'
NeoBundle 'fatih/vim-go'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'scrooloose/nerdcommenter'
NeoBundle 'rhysd/vim-clang-format'
NeoBundle 'ervandew/supertab'
NeoBundle 'google/vim-colorscheme-primary'

" Required:
call neobundle#end()

" Required:
filetype plugin indent on
" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck

let $NVIM_TUI_ENABLE_TRUE_COLOR=1
let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1

" Misc settings
let g:go_fmt_command = "goimports"
set background=light
let g:clang_format#detect_style_file = 1
let g:clang_format#auto_format = 1
